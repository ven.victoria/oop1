﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAccounting
{
    public class AuthUser
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public bool isAdmin { get; set; }
    }
}
