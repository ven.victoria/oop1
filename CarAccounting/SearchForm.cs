﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;

namespace CarAccounting
{
    public partial class SearchForm : Form
    {
        static string NameDB = "penalty.mdb";
        static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\" + NameDB;
        private OleDbConnection myConnection;
        private OleDbCommand myCommand;

        public SearchForm()
        {
            InitializeComponent();
            myConnection = new OleDbConnection(connectString);
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            string lpn_key = textBox1.Text;
            string carname_key = textBox2.Text;
            string ownername_key = textBox3.Text;
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT pa_id, pen_date, car_lpn, marka_name & ' ' & car_model, owner_fname & ' ' & owner_lname, pen_reason, pen_ispaid, pa_ishere " +
                                        "FROM [PenaltyArea], [Penalties], [Cars], [Owners], [Marks] " +
                                        "WHERE pen_car_id = car_id AND pa_penalty_id = pen_id AND car_owner_id = owner_id AND car_marka_id = marka_id AND " +
                                        ((lpn_key.Length > 0) ? "car_lpn LIKE '%" + lpn_key + "%' " : "1 ") +
                                        "AND " + ((carname_key.Length > 0) ? "marka_name & ' ' & car_model LIKE '%" + carname_key + "%' " : "1 ") +
                                        "AND " + ((ownername_key.Length > 0) ? "owner_fname & ' ' & owner_lname LIKE '%" + ownername_key + "%' " : "1 ");

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Дата", "Номер авто", "Автомобіль", "Власник", "Причина", "Оплачено", "На стоянці" };
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    dataGridView1.Columns[i].HeaderText = headerNames[i];
                    dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

    }
}
