﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace CarAccounting
{
    public partial class MainForm : Form
    {
        static string NameDB = "penalty.mdb";
        static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\" + NameDB;
        private OleDbConnection myConnection;
        private OleDbCommand myCommand;
        public static AuthUser currentUser = null;

        public MainForm()
        {
            InitializeComponent();
            myConnection = new OleDbConnection(connectString);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //input
            myConnection.Open();
            OleDbDataReader reader = (new OleDbCommand("SELECT * FROM Users", myConnection)).ExecuteReader();
            while (reader.Read())
                if (textBox1.Text == reader[3].ToString() && textBox2.Text == reader[4].ToString())
                    currentUser = new AuthUser() { userId = (int)reader[0], userName = reader[1] + " " + reader[2], isAdmin = ((string)reader[5] == "admin") };
            if(currentUser != null)
            {
                MenuForm fr1 = new MenuForm();
                this.Hide();
                fr1.ShowDialog();
            }

            myConnection.Close();
        }

    }
}
