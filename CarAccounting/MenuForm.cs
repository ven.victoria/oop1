﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using System.Data;

namespace CarAccounting
{
    public partial class MenuForm : Form
    {
        static string NameDB = "penalty.mdb";
        static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\" + NameDB;
        private OleDbConnection myConnection;
        private OleDbCommand myCommand;

        public MenuForm()
        {
            InitializeComponent();

            myConnection = new OleDbConnection(connectString);
        }

        private void MenuForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PAForm paForm = new PAForm();
            paForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //dovidnik
            DictionaryForm df1 = new DictionaryForm();
            df1.Show();
        }

        private void MyWordReplace(Word.Application app, string oldValue, string newValue)
        {
            Object missing = Type.Missing;
            Word.Find find = app.Selection.Find;
            find.Text = oldValue;
            find.Replacement.Text = newValue;
            Object wrap = Word.WdFindWrap.wdFindContinue;
            Object replace = Word.WdReplace.wdReplaceAll;
            find.Execute(FindText: Type.Missing,
                MatchCase: false,
                MatchWholeWord: false,
                MatchWildcards: false,
                MatchSoundsLike: missing,
                MatchAllWordForms: false,
                Forward: true,
                Wrap: wrap,
                Format: false,
                ReplaceWith: missing, Replace: replace);
        }

        private void WriteResultToFile()
        {
            Directory.CreateDirectory(Application.StartupPath + "\\Звіти");
            string outputFileName = Application.StartupPath + "\\Звіти\\" + "report_" + DateTime.Now.ToString().Replace(":", ".") + ".docx";
            File.Copy(Application.StartupPath + "\\" + "report.rpt", outputFileName);

            Word.Application wordApp = null;
            Word.Document wordDoc = null;
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT pa_id, pen_date, car_lpn, marka_name & ' ' & car_model, owner_fname & ' ' & owner_lname, pen_reason, pen_ispaid, pa_ishere " +
                                        "FROM [PenaltyArea], [Penalties], [Cars], [Owners], [Marks] " +
                                        "WHERE pen_car_id = car_id AND pa_penalty_id = pen_id AND car_owner_id = owner_id AND car_marka_id = marka_id";

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();
                da.Fill(dt);

                wordApp = new Word.Application();
                wordDoc = wordApp.Documents.Open(outputFileName);

                Word.Table table = wordDoc.Tables[1];
                Word.Row row = table.Rows[2];
                Object missing = Type.Missing;
                foreach (DataRow item in dt.Rows)
                {
                    item[1] = item[1].ToString().Substring(0, item[1].ToString().IndexOf(" "));
                    for(int i = 1; i < 8; i++)
                        table.Cell(row.Index, i).Range.Text = item[i].ToString();
                    table.Cell(row.Index, 6).Range.Text = (item[6].ToString() == "True") ? "Так" : "Ні";
                    table.Cell(row.Index, 7).Range.Text = (item[7].ToString() == "True") ? "Так" : "Ні";
                    row = table.Rows.Add(ref missing);
                }
                table.Rows.Last.Delete();

                wordApp.ActiveDocument.Save();
                string outputFilePdfName = outputFileName.Replace(".docx", ".pdf");
                wordApp.ActiveDocument.ExportAsFixedFormat(outputFilePdfName, Word.WdExportFormat.wdExportFormatPDF);
                wordApp.ActiveDocument.Close();
                wordApp.Quit();
                File.Delete(outputFileName);

                MessageBox.Show("Готово! Шлях: " + outputFilePdfName, "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WriteResultToFile();
            //exportToFile
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //searching
            SearchForm sf1 = new SearchForm();
            sf1.Show();
        }
    }
}
