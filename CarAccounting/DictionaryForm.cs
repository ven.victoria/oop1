﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;

namespace CarAccounting
{
    public partial class DictionaryForm : Form
    {
        static string NameDB = "penalty.mdb";
        static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\" + NameDB;
        private OleDbConnection myConnection;
        private OleDbCommand myCommand;
        private static DataGridViewRow myRow = null;

        public DictionaryForm()
        {
            InitializeComponent();
            myConnection = new OleDbConnection(connectString);
            buttonsDisable(this);
            inputs_clear(this);
            gridShow();
        }

        private void command(string cmd, string msg)
        {
            try
            {
                myConnection.Open();

                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;

                myCommand.CommandText = cmd;
                myCommand.ExecuteNonQuery();

                myConnection.Close();
                MessageBox.Show(msg, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
        }

        private void inputs_clear(Form page)
        {
            for (int i = 0; i < page.Controls.Count; i++)
                if (page.Controls[i].GetType().Name == "TextBox" || page.Controls[i].GetType().Name == "ComboBox" || page.Controls[i].GetType().Name == "DateTimePicker")
                    page.Controls[i].Text = "";
        }

        private void buttonsDisable(Form page)
        {
            for (int i = 0; i < page.Controls.Count; i++)
                if (page.Controls[i].GetType().Name == "Button")
                    page.Controls[i].Enabled = false;
        }

        private void gridShow()
        {
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT * FROM [Marks]";

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Назва"};
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    dataGridView1.Columns[i].HeaderText = headerNames[i];
                    dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                myConnection.Close();
                inputs_clear(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                myRow = dataGridView1.Rows[e.RowIndex];

                if (e.RowIndex + 1 == dataGridView1.RowCount)
                {
                    button1.Enabled = true;
                    button2.Enabled = false;
                    button3.Enabled = false;
                }
                else
                {
                    button1.Enabled = false;
                    button2.Enabled = true;
                    button3.Enabled = true;
                }
                textBox1.Text = myRow.Cells[1].Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string insert_query = "INSERT INTO [Marks] (marka_name) VALUES ('" + textBox1.Text + "')";
            string dublicate_query = "SELECT * FROM [Marks] WHERE marka_name = '" + textBox1.Text + "'";
            bool isEntered = true;
            if (textBox1.Text == "")
                isEntered = false;

            if (isEntered)
            {
                myCommand.CommandText = dublicate_query;

                DataTable st_table = new DataTable();
                OleDbDataAdapter st_adapter = new OleDbDataAdapter(myCommand);
                st_adapter.Fill(st_table);
                if (st_table.Rows.Count > 0)
                {
                    MessageBox.Show("Такий запис вже існує!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    myConnection.Close();
                    return;
                }
                else
                {
                    command(insert_query, "Запис добавлено!");
                    gridShow();
                    inputs_clear(this);
                }

            }
            else
            {
                MessageBox.Show("Заповніть усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string update_query = "UPDATE [Marks] SET marka_name = '" + textBox1.Text + "' WHERE marka_id = " + myRow.Cells[0].Value.ToString();
            bool isEntered = true;
            if (textBox1.Text == "")
                isEntered = false;

            if (isEntered)
            {
                command(update_query, "Запис оновлено!");
                gridShow();
            }
            else
                MessageBox.Show("Заповніть усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string delete_query = "DELETE FROM [Marks] WHERE marka_id = " + myRow.Cells[0].Value.ToString();
            command(delete_query, "Запис видалено!");
            gridShow();
        }
    }
}
