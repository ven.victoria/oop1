﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;

namespace CarAccounting
{
    public partial class PAForm : Form
    {
        static string NameDB = "penalty.mdb";
        static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\" + NameDB;
        private OleDbConnection myConnection;
        private OleDbCommand myCommand;
        private static DataGridViewRow myRow = null;
        private static int penaltyID = 0;

        public PAForm()
        {
            InitializeComponent();
            myConnection = new OleDbConnection(connectString);
        }

        private void combobox_fill(string cmd, string display_member, string value_member, ComboBox combo)
        {
            try
            {
                myConnection.Open();

                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = cmd;
                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable tab = new DataTable();

                da.Fill(tab);

                combo.DataSource = tab;
                combo.DisplayMember = display_member;
                combo.ValueMember = value_member;

                myConnection.Close();
                combo.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void command(string cmd, string msg)
        {
            try
            {
                myConnection.Open();

                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;

                myCommand.CommandText = cmd;
                myCommand.ExecuteNonQuery();

                myConnection.Close();
                MessageBox.Show(msg, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
        }

        private void inputs_clear(TabPage page)
        {
            for (int i = 0; i < page.Controls.Count; i++)
                if (page.Controls[i].GetType().Name == "TextBox" || page.Controls[i].GetType().Name == "ComboBox" || page.Controls[i].GetType().Name == "DateTimePicker")
                    page.Controls[i].Text = "";
        }

        private void ownerTabShow()
        {
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT * FROM [Owners]";

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                ownerGridView.DataSource = dt;

                ownerGridView.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Ім\'я", "Прізвище", "Паспорт", "Телефон" };
                for (int i = 0; i < ownerGridView.ColumnCount; i++)
                {
                    ownerGridView.Columns[i].HeaderText = headerNames[i];
                    ownerGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                myConnection.Close();

                inputs_clear(tabPage1);
                //ownerGridView_CellClick(this, new DataGridViewCellEventArgs(0, 0));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

        private void carTabShow()
        {
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT car_id, marka_name, car_model, car_lpn, car_VIN, owner_passport, owner_fname & ' ' & owner_lname " +
                                        "FROM [Cars], [Marks], [Owners] " +
                                        "WHERE car_marka_id = marka_id AND car_owner_id = owner_id";

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                carGridView.DataSource = dt;

                carGridView.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Марка", "Модель", "Номер", "VIN-код", "Паспорт власника", "Власник"};
                for (int i = 0; i < carGridView.ColumnCount; i++)
                {
                    carGridView.Columns[i].HeaderText = headerNames[i];
                    carGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                myConnection.Close();
                combobox_fill("SELECT * FROM [Marks]", "marka_name", "marka_id", comboBox1);
                //Expr1001
                combobox_fill("SELECT owner_id, owner_fname & ' ' & owner_lname AS owner_fullname, owner_passport, owner_phone FROM [Owners]", "owner_fullname", "owner_id", comboBox2);
                inputs_clear(tabPage2);
                //carGridView_CellClick(this, new DataGridViewCellEventArgs(0, 0));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

        private void penaltyPanelShow()
        {
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT pen_id, pen_date, car_lpn, owner_fname & ' ' & owner_lname, pen_reason, pen_ispaid " +
                                        "FROM [Penalties], [Cars], [Owners]" +
                                        "WHERE pen_car_id = car_id AND car_owner_id = owner_id";
         
                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                penaltyGridView.DataSource = dt;

                penaltyGridView.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Дата", "Автомобіль", "Власник", "Причина", "Оплачено" };
                for (int i = 0; i < penaltyGridView.ColumnCount; i++)
                {
                    penaltyGridView.Columns[i].HeaderText = headerNames[i];
                    penaltyGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                
                myConnection.Close();
                combobox_fill("SELECT * FROM [Cars]", "car_lpn", "car_id", comboBox3);
                inputs_clear(tabPage3);
                //penaltyGridView_CellClick(this, new DataGridViewCellEventArgs(0, 0));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

        private void paPanelShow()
        {
            try
            {
                myConnection.Open();
                myCommand = new OleDbCommand();
                myCommand.Connection = myConnection;
                myCommand.CommandText = "SELECT pa_id, pen_date, car_lpn, owner_fname & ' ' & owner_lname, pen_reason, pen_ispaid, pa_ishere " +
                                        "FROM [PenaltyArea], [Penalties], [Cars], [Owners] " +
                                        "WHERE pen_car_id = car_id AND pa_penalty_id = pen_id AND car_owner_id = owner_id";

                OleDbDataAdapter da = new OleDbDataAdapter(myCommand);
                DataTable dt = new DataTable();

                da.Fill(dt);
                paGridView.DataSource = dt;

                paGridView.Columns[0].Visible = false;

                string[] headerNames = { "ID", "Дата", "Автомобіль", "Власник", "Причина", "Оплачено", "На стоянці" };
                for (int i = 0; i < paGridView.ColumnCount; i++)
                {
                    paGridView.Columns[i].HeaderText = headerNames[i];
                    paGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                myConnection.Close();
                inputs_clear(tabPage4);
                //paGridView_CellClick(this, new DataGridViewCellEventArgs(0, 0));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }

        private void buttonsDisable(TabPage page)
        {
            for (int i = 0; i < page.Controls.Count; i++)
                if (page.Controls[i].GetType().Name == "Button")
                    page.Controls[i].Enabled = false;
        }

        private void myTabPage_Enter(object sender, EventArgs e)
        {
            string testText = "";
            switch(((TabPage)sender).Name)
            {
                case "tabPage1":
                    testText = "1";
                    ownerTabShow();
                    buttonsDisable(tabPage1);
                    break;
                case "tabPage2":
                    testText = "2";
                    carTabShow();
                    buttonsDisable(tabPage2);
                    break;
                case "tabPage3":
                    testText = "3";
                    penaltyPanelShow();
                    buttonsDisable(tabPage3);
                    break;
                case "tabPage4":
                    testText = "4";
                    paPanelShow();
                    buttonsDisable(tabPage4);
                    button12.Enabled = true;
                    break;
            }
            //this.Text = testText;
        }

        private void ownerGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                myRow= ownerGridView.Rows[e.RowIndex];

                if (e.RowIndex + 1 == ownerGridView.RowCount)
                {
                    button1.Enabled = true;
                    button2.Enabled = false;
                    button3.Enabled = false;
                }
                else
                {
                    button1.Enabled = false;
                    button2.Enabled = true;
                    button3.Enabled = true;
                }
                textBox1.Text = myRow.Cells[1].Value.ToString();
                textBox2.Text = myRow.Cells[2].Value.ToString();
                textBox3.Text = myRow.Cells[3].Value.ToString();
                textBox4.Text = myRow.Cells[4].Value.ToString();
            }
        }

        private void carGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                myRow = carGridView.Rows[e.RowIndex];

                if (e.RowIndex + 1 == carGridView.RowCount)
                {
                    button6.Enabled = true;
                    button5.Enabled = false;
                    button4.Enabled = false;
                }
                else
                {
                    button6.Enabled = false;
                    button5.Enabled = true;
                    button4.Enabled = true;
                }
                comboBox1.Text = myRow.Cells[1].Value.ToString();
                textBox5.Text = myRow.Cells[2].Value.ToString();
                textBox6.Text = myRow.Cells[3].Value.ToString();
                textBox7.Text = myRow.Cells[4].Value.ToString();
                comboBox2.Text = myRow.Cells[6].Value.ToString();
            }
        }

        private void penaltyGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                myRow = penaltyGridView.Rows[e.RowIndex];

                if (e.RowIndex + 1 == penaltyGridView.RowCount)
                {
                    button9.Enabled = true;
                    button8.Enabled = false;
                    button7.Enabled = false;
                    button13.Enabled = false;
                }
                else
                {
                    button9.Enabled = false;
                    button8.Enabled = true;
                    button7.Enabled = true;
                    button13.Enabled = true;
                }
                dateTimePicker1.Text = myRow.Cells[1].Value.ToString();
                comboBox3.Text = myRow.Cells[2].Value.ToString();
                textBox8.Text = myRow.Cells[4].Value.ToString();
                checkBox1.Checked = (e.RowIndex + 1 == penaltyGridView.RowCount) ? false : (bool)myRow.Cells[5].Value;
            }
        }

        private void paGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                myRow = paGridView.Rows[e.RowIndex];

                if (e.RowIndex + 1 == paGridView.RowCount)
                {
                    button12.Enabled = true;
                    button11.Enabled = false;
                    button10.Enabled = false;
                }
                else
                {
                    button12.Enabled = false;
                    button11.Enabled = true;
                    button10.Enabled = true;
                }
                dateTimePicker2.Text = myRow.Cells[1].Value.ToString();
                textBox9.Text = myRow.Cells[2].Value.ToString();
                textBox10.Text = myRow.Cells[3].Value.ToString();
                checkBox2.Checked = (e.RowIndex + 1 == paGridView.RowCount) ? false : (bool)myRow.Cells[6].Value;
            }
        }

        private void myAdding_Click(object sender, EventArgs e)
        {
            string insert_query = "";
            string dublicate_query = "";
            TabPage tabpg = null;

            switch (((Button)sender).Name)
            {
                case "button1":
                    tabpg = tabPage1;
                    insert_query = "INSERT INTO [Owners] (owner_fname, owner_lname, owner_passport, owner_phone) " +
                                   "VALUES ('" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" +
                                   textBox4.Text + "')";
                    dublicate_query = "SELECT * FROM [Owners] WHERE owner_passport = '" + textBox3.Text + "'";
                    break;
                case "button6":
                    tabpg = tabPage2;
                    insert_query = "INSERT INTO [Cars] (car_marka_id, car_model, car_lpn, car_VIN, car_owner_id) " +
                                   "VALUES ('" + comboBox1.SelectedValue + "', '" + textBox5.Text + "', " +
                                   "'" + textBox6.Text + "', '" + textBox7.Text + "','" + comboBox2.SelectedValue + "')";
                    dublicate_query = "SELECT * FROM [Cars] WHERE car_lpn = '" + textBox6.Text + "'";
                    break;
                case "button9":
                    tabpg = tabPage3;
                    insert_query = "INSERT INTO [Penalties] (pen_date, pen_car_id, pen_reason, pen_ispaid) " +
                                   "VALUES ('" + dateTimePicker1.Value.Date + "', '" + comboBox3.SelectedValue + "', " +
                                   "'" + textBox8.Text + "', " + (checkBox1.Checked ? 1 : 0) + ")";
                    dublicate_query = "SELECT * FROM [Penalties] WHERE pen_date = '" + dateTimePicker1.Value.Date + "' AND " +
                                      "pen_car_id = " + comboBox3.SelectedValue + " AND pen_reason = '" + textBox8.Text + "' " +
                                      "AND pen_ispaid = " + (checkBox1.Checked ? 1 : 0);
                    break;
                case "button12":
                    tabpg = tabPage4;
                    insert_query = "INSERT INTO [PenaltyArea] (pa_penalty_id, pa_ishere, pa_user_id) " +
                                   "VALUES ('" + penaltyID + "', " + (checkBox2.Checked ? 1 : 0) + ", '" + MainForm.currentUser.userId + "')";
                    dublicate_query = "SELECT * FROM [PenaltyArea] WHERE pa_penalty_id = " + penaltyID + "";
                    break;
            }

            bool isEntered = true;
            for (int i = 0; i < tabpg.Controls.Count; i++)
                if (tabpg.Controls[i].GetType().Name == "TextBox" || tabpg.Controls[i].GetType().Name == "ComboBox")
                    if (tabpg.Controls[i].Text == "")
                        isEntered = false;

            if (isEntered)
            {
                myCommand.CommandText = dublicate_query;

                DataTable st_table = new DataTable();
                OleDbDataAdapter st_adapter = new OleDbDataAdapter(myCommand);
                st_adapter.Fill(st_table);
                if (st_table.Rows.Count > 0)
                {
                    MessageBox.Show("Такий запис вже існує!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    myConnection.Close();
                    return;
                }
                else
                {
                    command(insert_query, "Запис добавлено!");
                    myTabPage_Enter(tabpg, e);
                    inputs_clear(tabpg);
                }

            }
            else
            {
                MessageBox.Show("Заповніть усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void myUpdating_Click(object sender, EventArgs e)
        {
            string update_query = "";
            TabPage tabpg = null;

            switch (((Button)sender).Name)
            {
                case "button2":
                    tabpg = tabPage1;
                    update_query = "UPDATE [Owners] SET owner_fname = '" + textBox1.Text + "', owner_lname = '" + textBox2.Text + "', " +
                                   "owner_passport = '" + textBox3.Text + "', owner_phone = '" + textBox4.Text + "' " +
                                   "WHERE owner_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button5":
                    tabpg = tabPage2;
                    update_query = "UPDATE [Cars] SET car_marka_id = '" + comboBox1.SelectedValue + "', car_model = '" + textBox5.Text + "', " +
                                   "car_lpn = '" + textBox6.Text + "', car_VIN = '" + textBox7.Text + "', car_owner_id = '" + comboBox2.SelectedValue + "' " +
                                   "WHERE car_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button8":
                    tabpg = tabPage3;
                    update_query = "UPDATE [Penalties] SET pen_date = '" + dateTimePicker1.Value.Date + "', pen_car_id = '" + comboBox3.SelectedValue + "', " +
                                   "pen_reason = '" + textBox8.Text + "', pen_ispaid = " + (checkBox1.Checked ? 1 : 0) + " " +
                                   "WHERE pen_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button11":
                    tabpg = tabPage4;
                    update_query = "UPDATE [PenaltyArea] SET pa_ishere = " + (checkBox2.Checked ? 1 : 0) + ", " +
                                   "pa_user_id = '" + MainForm.currentUser.userId + "' " +
                                   "WHERE pa_id = " + myRow.Cells[0].Value.ToString();
                    break;
            }

            bool isEntered = true;
            for (int i = 0; i < tabpg.Controls.Count; i++)
                if (tabpg.Controls[i].GetType().Name == "TextBox" || tabpg.Controls[i].GetType().Name == "ComboBox")
                    if (tabpg.Controls[i].Text == "")
                        isEntered = false;

            if (isEntered)
            {
                command(update_query, "Запис оновлено!");
                myTabPage_Enter(tabpg, e);
                inputs_clear(tabpg);
            }
            else
            {
                MessageBox.Show("Заповніть усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void myDeleting_Click(object sender, EventArgs e)
        {
            string delete_query = "";
            TabPage tabpg = null;

            switch (((Button)sender).Name)
            {
                case "button3":
                    tabpg = tabPage1;
                    delete_query = "DELETE FROM [Owners] WHERE owner_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button4":
                    tabpg = tabPage2;
                    delete_query = "DELETE FROM [Cars] WHERE car_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button7":
                    tabpg = tabPage3;
                    delete_query = "DELETE FROM [Penalties] WHERE pen_id = " + myRow.Cells[0].Value.ToString();
                    break;
                case "button10":
                    tabpg = tabPage4;
                    delete_query = "DELETE FROM [PenaltyArea] WHERE pa_id = " + myRow.Cells[0].Value.ToString();
                    break;
            }

            command(delete_query, "Запис видалено!");
            myTabPage_Enter(tabpg, e);
            inputs_clear(tabpg);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            penaltyID = (int)myRow.Cells[0].Value;
            tabControl1.SelectedTab = tabPage4;

            paGridView.Rows[paGridView.Rows.Count - 1].Selected = true;

            dateTimePicker2.Value = dateTimePicker1.Value;
            textBox9.Text = myRow.Cells[2].Value.ToString();
            textBox10.Text = myRow.Cells[3].Value.ToString();
        }
    }
}
